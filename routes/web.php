<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/checkout', ['as' => 'checkout.store', 'uses' => 'CheckoutController@store']); //post anything!
Route::get('/checkout/cancel', ['as' => 'cancel', 'uses' => 'CheckoutController@cancel']);
Route::get('/checkout/success', ['as' => 'success', 'uses' => 'CheckoutController@success']);
