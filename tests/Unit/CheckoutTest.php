<?php

namespace Tests\Unit;

use App\Models\Events;
use Tests\TestCase;
use Auth;

class CheckoutTest extends TestCase
{
    /**
     * User with balance checkout single game
     *
     * @return void
     */

    /** @test */
    public function playerCheckoutSingleEvent()
    {
        $event = factory(Events::class)->create();
        $data = [
            'purchase_name' => '1',
            'purchase_type' => 'event',
            'purchase_desc' => 'For event details check your inbox or basketball613.com.au',
            'payment_method' => 'PayPal_Express',
            'items' => "[{'sku':" . $event->id . ",'price':,'guests':[],'detail':{'local':{'id':" . $event->id . ",'name':'5on5 Intermediate- (CR & LC) [Court#1 Downstairs]','description':'<p>Weekly intermediate basketball for regular players!</p> <p>Event guide <a href='https://basketball613.com.au/weekly-basketball-game-guide' class='linkified'>https://basketball613.com.au/weekly-basketball-game-guide</a></p> <p>Want to bring a team? - <a href='https://basketball613.com.au/team' class='linkified'>https://basketball613.com.au/team</a></p> <p>Facilities: Showers, Parking, Trains, Drinking Water.</p> <p>We have EASY social games for all players and beginners</p>','status':'upcoming','time':{'date':'2021-02-11 20:00:00.000000','timezone_type':3,'timezone':'Australia/Melbourne'},'response_limit':'14','fee_amount':'15','fee_amount_team':'60','no_discount':'0','featured':'1','venue_id':'12','visibility':','venue_visibility':','meetup_event_id':','response_count':'12','photo_url':','guest_limit':','created_at':'2019-09-24 11:27:52','updated_at':'2021-02-10 20:50:43','competition':'0','list_id':null,'hosts':null,'time_start':'-0001-11-30 00:00:00','time_end':'-0001-11-30 00:00:00','challonge_id':null,'hashtag':'thu2030footscray','slug':null,'group':'LC','type':','scope':null,'deleted_at':null,'notes':','template_id':'15','space':2,'hasGame':'false','feeDiscounted':12,'authCheck':false,'response':null,'teams':[]}}}]",
            'charset' => 'utf-8',
            'sub_total' => '12',
            'discount' => '0',
            'purchase_discount' => '0',
            '_token' => csrf_token(),
        ];

        $user = Auth::loginUsingId(1740);

        $result = $this->actingAs($user)->post('/checkout', $data);
        $this->assertEquals(200, $result->response->getStatusCode());
        // ['X-Requested-With' => 'XMLHttpRequest']
        // $response = $result->response; //->getOriginalContent();
        // $this->assertStringContainsString($result->response, 'Redirecting to');
        // $result->response->assertSeeText('Redirecting to');
    }
}
