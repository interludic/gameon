<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;
use Omnipay\Omnipay;

class Checkout extends Model
{
    /**
     * Get the gateway
     *
     * @return gateway
     * @author
     **/
    public function getGateway()
    {
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('PAYMENT_SANDBOX_PAYPAL_USERNAME');
        $gateway->setPassword('PAYMENT_SANDBOX_PAYPAL_PASSWORD');
        $gateway->setSignature('PAYMENT_SANDBOX_PAYPAL_SIG');
        $gateway->setTestMode(true);

        return $gateway;
    }

    /**
     * Processing done for each item upon successful checkout function
     *
     * @return void
     * @author
     **/
    public function success($order, $items = null)
    {

        try {
            echo "Mock success code coverage complete";
            // TODO move to observer for completed order
            // $user->notify(new AdminInvoicePaid($order));
        } catch (\Exception $e) {
            Log::error('Could not ' . $e->getMessage());
        }

        return true;
    }
}
