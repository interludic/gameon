<?php

namespace App\Http\Controllers;


use App\Models\Checkout;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Log;

class CheckoutController extends Controller
{
    /**
     * Store session variables, process purchase
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckoutRequest $request)
    {
        Session::put('items', $request['items']);
        Session::put('params', $request['params']);
        Session::save();
        $checkout = new Checkout();

        $gateway = $checkout->getGateway(); //'PayPal_Express'
        $response = $gateway->purchase($request['params'])->send();

        if ($response->isSuccessful()) {
            //NOT PAYPAL //https://github.com/thephpleague/omnipay // payment was successful: update database
        } elseif ($response->isRedirect()) {
            // dd($request);
            $response->redirect(); // redirect to offsite payment gateway
        } else {
            Log::error(print_r($response->getMessage(), true));

            //MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!MOCK!
            dump('Payment Gateway Error', 'Looks like there is a problem contacting the gateway, try again later.');
            // return redirect()->route('members-area.home.index');
            return redirect()->back();
        }
    }

    public function success(Request $request)
    {
        $params = Session::get('params');
        if (!$params) {
            abort('400', 'Your session was lost, please try again!');
        } //echo "the domain has changed and session is lost or direct route access, so redirect";
        $items = Session::get('items');
        $checkout = new Checkout();
        $gateway = $checkout->getGateway($params['payment_method']);
        $response = $gateway->completePurchase($params)->send();

        if ($response->isRedirect()) { //https://github.com/thephpleague/omnipay-paypal/pull/144
            Log::warning('User redirected back to PayPal', ['response' => print_r($response, true)]);
            $response->getRedirectUrl();
        }

        $gatewayData = $response->getData();

        if (isset($gatewayData['PAYMENTINFO_0_ACK']) && $gatewayData['PAYMENTINFO_0_ACK'] === 'Success') {

            $checkout->success('order#1', $items);

            Log::info('Checkout Success items, ');
        } else {
            Log::error('Checkout failure', [
                // 'response' => print_r($response, true),
                'gateway' => print_r($gatewayData, true),
            ]);

            dump((isset($gatewayData['L_LONGMESSAGE0']) ? $gatewayData['L_LONGMESSAGE0'] : null), isset($gatewayData['L_SHORTMESSAGE0']) ? $gatewayData['L_SHORTMESSAGE0'] : null)->persistent('Got it');
            abort('402', 'Payment required');
        }


        dd('return redirect()->route()');
    }

    public function index(Request $request)
    {
        //show cart
        //Alternative payment method / stripe.
        abort('404', 'Page not found');
    }

    public function cancel(Request $request)
    {
        abort('402', 'Payment required');
    }
}
