<?php

namespace App\Http\Requests;

use App\Models\AdminSetting;
use Illuminate\Foundation\Http\FormRequest;

use Auth;

class CheckoutRequest extends FormRequest
{
    // public function forbiddenResponse()
    // {
    //     return abort(403);
    // }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return Auth::check();
    // }

    protected function getValidatorInstance()
    {

        $paypalUrl = 'http://localhost:8000/checkout/'; //env('PAYMENT_SANDBOX_PAYPAL_URL');

        // $request = $this->all();

        $items = ['1' => 'data'];
        if (!is_array($items)) {
            $items = [$items];
        }

        $params = [
            'returnUrl'     => $paypalUrl . 'success',
            'cancelUrl'     => $paypalUrl . 'cancel',
            'name'      => 'purchase_name',
            'description'   => 'purchase_name' . ' - ' . 'purchase_desc',
            'amount'    => floatval(1),
            'currency'  => 'AUD',
            'type'      => 'purchase_type',
            'payment_method' => 'payment_method',
            'discount'  => 'purchase_discount', //Discount rate?
            'userId'        => 1,
        ];
        $data = [
            'items' => $items,
            'params' => $params,
        ];

        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/

        return parent::getValidatorInstance();
    }

    public function rules()
    {
        return [];
    }
}
